TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Bubble.cpp \
    BubbleMelhorado.cpp \
    Controle.cpp \
    Heap.cpp \
    Insertion.cpp \
    Merge.cpp \
    Quick.cpp \
    Selection.cpp \
    Radix.cpp \
    Arquivo.cpp

HEADERS += \
    Selection.h \
    Radix.h \
    Quick.h \
    Merge.h \
    Insertion.h \
    Heap.h \
    Controle.h \
    BubbleMelhorado.h \
    Bubble.h \
    Arquivo.h

