Definições:

1. Equipes de no máximo dois acadêmicos e, caso o número seja impar, uma equipe
poderá conter três acadêmicos.

2. Linguagem C++ ou Java.

3. Implementar os seguintes algoritmos: BubleSort, BubleSortMelhorado, InsertionSort,
SelectionSort, QuickSort, MergeSort, HeapSort e RadixSort.

4. Para cada algoritmo, em cada instrução, contabilizar o custo de execução.

5. Os vetores contendo os números que deverão ser ordenados serão entregues no dia
28/05.

6. O programa que ordena os números deverá seguir os passos:

Ao ser executado, solicitar o nome do arquivo que contém a lista dos arquivos com
os números. Exemplo:
Dados.txt
Dados1.txt
Dados2.txt
Dados3.txt
...
Dadosn.txt


Conteúdo do arquivo exemplo dados1.txt:
Dados1.txt
x1
x2
x3
...
xn
Onde x simboliza um número inteiro.


O programa deverá ler cada arquivo e submeter os dados para cada algoritmo de
ordenação;
Documentar os métodos do código fonte (funcionalidades dos métodos e a
descrição dos parâmetros);

7. Criar um arquivo relatorio.txt contendo as seguintes informações:
UNIOSTE – Universidade Estadual do Oeste do Paraná
Campus de Foz do Iguaçu/PR
Centro de Engenharias e Ciências Exatas
Curso de Ciência da Computação
Nome do Grupo, linguagem e unidade de tempo
Bubble Sort Bubble sort melhorado Insertion Sort
.... ... RadixSort
dados1 .txt
dados2 .txt
...
dadosn.txt

8. Encaminhar todo o projeto e o relatório para o e-mail teresinha.arnauts@gmail.com.

9. As apresentações dos trabalhos serão realizadas nos dias __/__/2014 e __/__/2014, e
deverão utilizar slides contendo:
 Equipe desenvolvedora;
 Materiais utilizados;
 Descrição de como cada membro da equipe contribuiu no desenvolvimento do
trabalho;
 No final da apresentação, deverá ser demonstrada a execução com um novo
conjunto de arquivo que será fornecido no momento da apresentação.