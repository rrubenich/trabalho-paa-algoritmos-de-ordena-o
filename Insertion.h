#ifndef INSERTION_H
#define INSERTION_H

#include "Controle.h";

class Insertion{
public:
    Insertion();

    void InsertionSort(int v[], int n);
    Controle getControle();

private:
    Controle controle;
};

#endif // INSERTION_H
