#ifndef HEAP_H
#define HEAP_H

#include "Controle.h";

class Heap{
public:
    Heap();
    void MaxHeapfy(int v[], int i, int n,int &count);
    void HeapSort(int v[], int n);
    void swap(int &i, int &j);
    Controle getControle();

private:
    Controle controle;
};

#endif // HEAP_H
