#ifndef AUXILIAR_H
#define AUXILIAR_H

class Auxiliar{
public:
    Auxiliar(){

    }


    void swap(int *i, int *j){
        int* aux = i;
        i = j;
        j = aux;
    }

};

#endif // AUXILIAR_H
