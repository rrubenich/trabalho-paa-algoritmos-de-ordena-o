#include "Heap.h";
#include <time.h>

Heap::Heap(){

}

void Heap::swap(int &i, int &j){
    int aux = i;
    i = j;
    j = aux;
}

void Heap::MaxHeapfy(int v[], int i, int n,int &count){
    int maior = i; count++;
    int left = i*2+1; count++;
    int right = i*2+2; count++;

    if(left < n && v[i] < v[left]){
        count+3;
        maior = left;
    }
    if(right < n && v[i] < v[right]){
        count+3;
        maior = right;
    }
    if(i != maior){
        swap(v[i],v[maior]);
        count+4;
        MaxHeapfy(v, maior, n, count);
    }
}

void Heap::HeapSort(int v[], int n){
    int count = 0;
    clock_t inicio, fim;

    inicio = clock();

    int i;
    for (i = (n-1/2), count++;(i >= 0) && count++;i--, count++)
        MaxHeapfy(v,i,n, count);
    do{
        swap(v[0],v[--n]); count+=3;
        MaxHeapfy(v,0,n, count);
    }
    while(n > 0);

    fim = clock();

    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Heap::getControle(){
    return this->controle;
}
