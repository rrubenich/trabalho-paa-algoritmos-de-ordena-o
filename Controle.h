#ifndef CONTROLE_H
#define CONTROLE_H

class Controle{
public:
    Controle();
    int getCusto();
    void setCusto(int custo);
    double getTempo();
    void setTempo(double tempo);

private:
    int custo;
    double tempo;
};

#endif // CONTROLE_H
