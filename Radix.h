#ifndef RADIX_H
#define RADIX_H
#include "Controle.h"

class Radix{
public:
    Radix();
    void RadixSort(int *a, int n);
    Controle getControle();

private:
    Controle controle;
};

#endif // RADIX_H
