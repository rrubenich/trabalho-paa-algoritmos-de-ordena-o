#include "Insertion.h";
#include <time.h>

Insertion::Insertion(){

}

void Insertion::InsertionSort(int v[], int n) {
    int i , j, key, count;
    count = 0;
    clock_t inicio, fim;

    inicio = clock();

    for(i=1, count++;(i < n)&& count++;i++, count++){
            key = v[i]; count++;
            for(j=i-1, count++;(j>=0)&&(key<v[j]) && count++;j--, count++){
                v[j+1]=v[j]; count++;
            }
            v[j+1]=key; count++;
        }

    fim = clock();

    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Insertion::getControle(){
    return this->controle;
}
