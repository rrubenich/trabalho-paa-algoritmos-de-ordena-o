#ifndef ARQUIVO_H
#define ARQUIVO_H

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class Arquivo{
private:

public:
    Arquivo();
    vector<string> leArquivoPrincipal(string nomeArquivo);
    void leArquivo(string nomeArquivo, int v[]);
};

#endif // ARQUIVO_H
