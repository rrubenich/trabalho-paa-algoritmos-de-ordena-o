#include "Quick.h";
#include <time.h>

Quick::Quick(){
    int count = 0;
}

void Quick::swap(int &i, int &j){
    int aux = i;
    i = j;
    j = aux;
}

int Quick::Partition(int v[],int l, int r, int &count){
    int pivo = v[l]; count++;
    int i = l;count++;
    int j = r+1;count++;


    while(1){
        count++;
        while (v[++i] < pivo){
            count+2;
            if (i == r) break;
        }
        while (pivo < v[--j]){
            count+2;
            if (j == l) break;
        }
        if (i >= j) break; count++;

        swap(v[i],v[j]); count+3;
    }

    swap(v[l],v[j]);count+3;
    return j;
}

void Quick::QuickSort(int v[],int l, int r, int &count){
    if(l < r){
        count++;
        int m = Partition(v,l,r, count);
        QuickSort(v,l,m-1, count);
        QuickSort(v,m+1,r, count);
    }
}

void Quick::QuickSortControlado(int v[], int inicio, int fim){
    int count = 0;
    clock_t inicioRel, fimRel;

    inicioRel = clock();
    QuickSort(v, inicio, fim, count);
    fimRel = clock();

    this->controle.setTempo((fimRel-inicioRel)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Quick::getControle(){
    return this->controle;
}

