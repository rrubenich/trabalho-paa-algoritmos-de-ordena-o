#include "Selection.h";
#include <time.h>

Selection::Selection(){

}

void Selection::swap(int &i, int &j){
    int aux = i;
    i = j;
    j = aux;
}


void Selection::SelectionSort(int v[], int n){
    int count = 0;
    clock_t inicio, fim;

    inicio = clock();

    int aux, i, j;
    for(i = 0, count++; (i < n-1) && count++; i++, count++){
        aux = i; count++;
        for(j = i+1, count++;(j < n) && count++;j++, count++){

            count++;

            if(v[aux] > v[j]){
                aux = j;
                count+=2;
            }
        }

        swap(v[i],v[aux]); count+=3;
    }

    fim = clock();
    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Selection::getControle(){
    return this->controle;
}
