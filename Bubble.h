#ifndef BUBBLE_H
#define BUBBLE_H

#include "Controle.h";

class Bubble{
public:
    Bubble();

    void swap(int &i, int &j);
    void BubbleSort(int a[], int n);
    Controle getControle();

private:
    Controle controle;
};

#endif // BUBBLESORT_H
