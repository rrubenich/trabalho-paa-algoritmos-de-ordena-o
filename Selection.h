#ifndef SELECTION_H
#define SELECTION_H

#include "Controle.h";

class Selection{
public:
    Selection();

    void swap(int &i, int &j);
    void SelectionSort(int v[], int n);
    Controle getControle();

private:
    Controle controle;
};

#endif // SELECTION_H
