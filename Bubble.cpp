#include "Bubble.h"
#include <time.h>

Bubble::Bubble(){

}

void Bubble::swap(int &i, int &j){
    int aux = i;
    i = j;
    j = aux;
}


void Bubble::BubbleSort(int v[], int n){
    int i, j, count;
    count = 0;
    clock_t inicio, fim;

    inicio = clock();
    for(i = 0, count++; (i<n-1) && count++; i++, count++){
        for(j = 0, count++; (j < n-i) && count++; j++, count++){
            if(v[j]>v[j+1] && count++){
                count += 3;
                swap(v[j],v[j+1]);
            }
        }
    }
    fim = clock();


    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Bubble::getControle(){
    return this->controle;
}
