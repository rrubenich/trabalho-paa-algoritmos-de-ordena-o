#ifndef BUBBLESORTMELHORADO_H
#define BUBBLESORTMELHORADO_H

#include "Controle.h";

class BubbleMelhorado{
public:
    BubbleMelhorado();

    void swap(int &i, int &j);
    void BubbleSortMelhorado(int a[], int n);
    Controle getControle();

private:
    Controle controle;
};

#endif // BUBBLESORTMELHORADO_H
