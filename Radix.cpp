#include "Radix.h"
#include <time.h>

Radix::Radix(){

}

void Radix::RadixSort(int *a, int n){
    int count = 0;
    clock_t inicio, fim;

    inicio = clock();

    int i, b[n];
    int m = a[0];
    int exp = 1;


    for (i = 1, count++; (i < n) && count++; i++, count++) {
        if (a[i] > m){
            m = a[i];
            count+2;
        }
    }

    while (m / exp > 0){
        count++;
        int bucket[10] = {0}; count++;

        for (i = 0, count++; (i < n) && count++; i++, count++){
            bucket[(a[i] / exp) % 10]++;
            count++;
        }
        for (i = 1, count++; (i < 10) && count++; i++, count++){
            bucket[i] += bucket[i - 1];
            count++;
        }
        for (i = n - 1, count++; (i >= 0) && count++; i--, count++){
            b[--bucket[(a[i] / exp) % 10]] = a[i];
            count++;
        }
        for (i = 0, count++; (i < n) && count++; i++, count++){
            a[i] = b[i];
            count++;
        }

        exp *= 10;
        count++;
    }

    fim = clock();

    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Radix::getControle(){
    return this->controle;
}
