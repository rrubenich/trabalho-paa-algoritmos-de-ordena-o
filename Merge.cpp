#include "Merge.h"
#include <time.h>
#include <iostream>

using namespace std;

Merge::Merge(){

}

int Merge::Intercala(int v[], int inicio, int meio, int fim){
    int i, j, k, count;
    count = 0;
    i = inicio; count++;
    j = meio; count++;
    k = 0; count++;

    int w[i-j]; count++;
    while ((i < meio) && (j < fim)){
        if (v[i] <= v[j]){
           w[k++] = v[i++];
           count+4;
        }
        else{
           w[k++] = v[j++];
           count+3;
        }
    }

    while(i < meio){
        w[k++] = v[i++];
        count+2;
    }

    while(j < fim){
        w[k++] = v[j++];
        count+2;
    }
    for (i = inicio, count++; (i < fim) && count++; ++i, count++){
        v[i] = w[i-inicio];
        count++;
    }
    return count;
}


void Merge::MergeSort(int v[], int inicio, int fim, int &count){
    if (inicio < fim-1){
        int meio = (inicio + fim)/2;
        count++;
        MergeSort(v, inicio, meio, count);
        MergeSort(v, meio, fim, count);
        count += Intercala(v, inicio, meio, fim);
    }
}

void Merge::MergeSortControlado(int v[], int inicio, int fim){
    int count = 0;
    clock_t inicioRel, fimRel;

    inicioRel = clock();
    MergeSort(v, inicio, fim, count);
    fimRel = clock();

    this->controle.setTempo((fimRel-inicioRel)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle Merge::getControle(){
    return this->controle;
}
