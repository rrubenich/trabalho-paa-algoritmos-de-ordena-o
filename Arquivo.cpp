#include "Arquivo.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

Arquivo::Arquivo(){

}

vector<string> Arquivo::leArquivoPrincipal(string nomeArquivo){

    vector<string> retorno;
    ifstream arquivo;
    arquivo.open(nomeArquivo.c_str());

    string linha;
    arquivo >> linha;

    while(!arquivo.eof()){
        retorno.push_back(linha);
        arquivo >> linha;
    }
    return retorno;
}

void Arquivo::leArquivo(string nomeArquivo, int v[]){

    int i = 0;

    ifstream arquivo;
    arquivo.open(nomeArquivo.c_str());

    string linha;
    arquivo >> linha;

    while(!arquivo.eof()){

        string s = linha;
        int linhaInt = std::atoi(s.c_str());

        v[i++] = linhaInt;
        arquivo >> linha;
    }
}
