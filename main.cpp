#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

#include "Arquivo.h";

#include "Bubble.h"
#include "BubbleMelhorado.h";
#include "Heap.h"
#include "Insertion.h";
#include "Merge.h";
#include "Quick.h";
#include "Radix.h"
#include "Selection.h"

#define tamanho 7


using namespace std;

const string dir = "Vetores/v7/";
const string dirArq = "Vetores/v7/arquivo7.txt";

int main(){

    int ordenado[tamanho-1];

    Arquivo file = Arquivo();
    vector<string> arquivos;

    arquivos = file.leArquivoPrincipal(dirArq);
    file.leArquivo(dir+arquivos.at(54), ordenado);

    for(int i = 0; i < tamanho; i++){
        cout << ordenado[i] << endl;
    }


    cout << "-----------"<< endl;

    Heap asd = Heap();

    asd.HeapSort(ordenado,tamanho-1);

    for(int i = 0; i < tamanho; i++){
        cout << ordenado[i] << endl;
    }


    cout << "custo " << asd.getControle().getCusto() << endl;
    cout << "tempo " << asd.getControle().getTempo() << endl;

    return 0;
}

