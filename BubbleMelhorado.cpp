#include "BubbleMelhorado.h"
#include <time.h>

BubbleMelhorado::BubbleMelhorado(){

}

void BubbleMelhorado::swap(int &i, int &j){
    int aux = i;
    i = j;
    j = aux;
}

void BubbleMelhorado::BubbleSortMelhorado(int v[], int n){
    int i, j, count;
    int troca = 1;
    count = 0;
    clock_t inicio, fim;

    inicio = clock();
    for (j = n-1, count++; (j>=1) && troca && count++; j--, count++){
        troca = 0;
        count++;

        for (i = 0, count++; i < j && count++; i++, count++){
            if (v[i]>v[i+1] && count++){
                count+=4;
                swap(v[j],v[j+1]);
                troca = 1;
            }
        }
    }
    fim = clock();

    this->controle.setTempo((fim-inicio)*1000/CLOCKS_PER_SEC);
    this->controle.setCusto(count);
}

Controle BubbleMelhorado::getControle(){
    return this->controle;
}
