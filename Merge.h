#ifndef MERGE_H
#define MERGE_H
#include <Controle.h>

class Merge{
public:
    Merge();

    void MergeSort(int v[], int inicio, int fim,int &count);
    int Intercala(int v[], int inicio, int meio, int fim);
    void MergeSortControlado(int v[], int inicio, int fim);
    Controle getControle();

private:
    Controle controle;
};

#endif // MERGE_H
