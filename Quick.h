#ifndef QUICK_H
#define QUICK_H
#include <Controle.h>

class Quick{
public:
    Quick();

    void QuickSort(int v[],int l, int r, int &count);
    int Partition(int v[],int l, int r, int &count);
    void swap(int &i, int &j);
    void QuickSortControlado(int v[], int inicio, int fim);
    Controle getControle();

private:
    Controle controle;
    int count;
};

#endif // QUICK_H
